#include <stdlib.h>
#include <stdio.h>

typedef struct no {
    int cont;
    struct no *esq;
    struct no *dir;
    struct no *pai;
} abb;

abb *Abb (int cont, abb *esq, abb *dir, abb *pai){
    abb *nova = malloc(sizeof(abb));
    nova->cont = cont;
    nova->esq = esq;
    nova->dir = dir;
    nova->pai = pai;
    return nova;
}

abb *insere (abb *arv, int cont){
    if (arv == NULL) return Abb(cont, NULL, NULL, NULL);
    if (arv->cont > cont) {
        arv->esq = insere(arv->esq, cont);
        arv->esq->pai = arv;
    } else {
        arv->dir = insere(arv->dir, cont);
        arv->dir->pai = arv;
    }
    return arv;
}

abb *busca (abb *arv, int k){
    if (arv == NULL || arv->cont == k) return arv;
    if (arv->cont > k) return busca(arv->esq, k);
    else return busca(arv->dir, k);
}

void printa(abb * arv){
    if (arv != NULL) {
        printa(arv->esq);
        printf("%d, ", arv->cont);
        printa(arv->dir);
    }
}

void print_pre(abb * arv){
    if (arv != NULL) {
        printf("%d, ", arv->cont);
        print_pre(arv->esq);
        print_pre(arv->dir);
    }
}

void print_pos(abb * arv){
    if (arv != NULL) {
        print_pos(arv->esq);
        print_pos(arv->dir);
        printf("%d, ", arv->cont);
    }
}

abb *min(abb *arv){
    while (arv->esq != NULL) 
        arv = arv->esq;
    return arv;
}

abb *max(abb *arv){
    while (arv->dir != NULL) 
        arv = arv->dir;
    return arv;
}

abb *prox(abb *arv){
    if (arv->dir != NULL) return min(arv->dir);
    while (arv->pai != NULL && arv->pai->dir == arv)
        arv = arv->pai;
    return arv->pai;
}

abb *ante(abb *arv){
    if (arv->esq != NULL) return max(arv->esq);
    while (arv->pai != NULL && arv->pai->esq == arv)
        arv = arv->pai;
    return arv->pai;
}

int main () {
    abb *arvore = NULL;
    arvore = insere(arvore, 4);
    arvore = insere(arvore, 2);
    arvore = insere(arvore, 6);
    arvore = insere(arvore, 1);
    arvore = insere(arvore, 3);
    arvore = insere(arvore, 5);
    arvore = insere(arvore, 7);

    // printf("central: ");
    // printa(arvore);
    // printf("\n");
    // printf("prefixo: ");
    // print_pre(arvore);
    // printf("\n");
    // printf("posfixo: ");
    // print_pos(arvore);
    // printf("\n");
    

    int k = 7;
    printf("Busca %d\n", k);

    abb *encontrado = busca(arvore, k);
    if (encontrado == NULL) printf("Não achou!!!\n");
    else {
        printf("Achou!\n");
        if (encontrado->pai == NULL) {
            printf("O elemento %d e raiz.\n", k);
        } else {
            printf("O pai do %d e o %d .\n",k, encontrado->pai->cont);
        }
        
    }

    printf("Max: %d \n", max(arvore)->cont);
    printf("Min: %d \n", min(arvore)->cont);

    printf("\n\n");

    abb *inicio = min(arvore);
    while (inicio != NULL) {
        printf("%d, ", inicio->cont);
        inicio = prox(inicio);
    }

    printf("\n\n");
    abb *fim = max(arvore);
    while (fim != NULL) {
        printf("%d, ", fim->cont);
        fim = ante(fim);
    }
    
    return 0;
}